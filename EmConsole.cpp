#include "EmConsole.h"

using namespace std;

/***Constructers***/

Console::Console(string title)
{
	SetConsoleTitle(title.c_str());
	cursorPosition.X = 0;
	cursorPosition.Y = 0;
	//size.Top = 0;
	//size.Left = 0;
	//size.Bottom = 20;
	//size.Right = 20;
	con[0] = CreateConsoleScreenBuffer(GENERIC_READ | GENERIC_WRITE, FILE_SHARE_READ | FILE_SHARE_WRITE, NULL, CONSOLE_TEXTMODE_BUFFER, NULL);
	con[1] = CreateConsoleScreenBuffer(GENERIC_READ | GENERIC_WRITE, FILE_SHARE_READ | FILE_SHARE_WRITE, NULL, CONSOLE_TEXTMODE_BUFFER, NULL);
	//SetConsoleActiveScreenBuffer(con[0]);
}

Console::Console()
{
	SetConsoleTitle("New Window");
	cursorPosition.X = 0;
	cursorPosition.Y = 0;
	//size.Top = 0;
	//size.Left = 0;
	//size.Bottom = 20;
	//size.Right = 20;
	con[0] = CreateConsoleScreenBuffer(GENERIC_READ | GENERIC_WRITE, FILE_SHARE_READ | FILE_SHARE_WRITE, NULL, CONSOLE_TEXTMODE_BUFFER, NULL);
	con[1] = CreateConsoleScreenBuffer(GENERIC_READ | GENERIC_WRITE, FILE_SHARE_READ | FILE_SHARE_WRITE, NULL, CONSOLE_TEXTMODE_BUFFER, NULL);
}

/***Functions**/

//Creates a vector of vector string (2D vector) from a txt file
vector<vector<string>> Console::textFileToVector(string file)
{
	fstream ascii(file, fstream::in);
	vector<vector<string>> str;

	if (ascii.is_open())
		OutputDebugString("enter\n");
	else
		OutputDebugString("no entry\n");

	if (ascii.is_open())
	{
		string line;
		str.push_back(vector<string>());
		for (int a = 0; getline(ascii, line);)
		{
			if (line.size() == 0)
			{
				a++;
				str.push_back(vector<string>());
			} else if (line.size() > 0)
				str[a].push_back(line);
			OutputDebugString((line + "\n").c_str());
		}
		ascii.close();
	}
	
	return str;
}

//Sets title
void Console::setTitle(string title)
{
	SetConsoleTitle(title.c_str());
}

//Sets the console size
void Console::setSize(float x, float y)
{
	sz = GetLargestConsoleWindowSize(GetStdHandle(STD_OUTPUT_HANDLE));
	size.Top = 0;
	size.Left = 0;
	size.Bottom = y-1 ;
	size.Right = x-1 ;

	sz.X = x - 1 < (sz.X - 1) ? x - 1 : (sz.X - 1);
	sz.Y = y - 1 < (sz.Y - 1) ? y - 1 : (sz.Y - 1);

	SetConsoleScreenBufferSize(con[1], sz);
	SetConsoleWindowInfo(con[1], true, &size);

	SetConsoleScreenBufferSize(con[0], sz);
	SetConsoleWindowInfo(con[0], true, &size);
}

//Sets console cursor position
void Console::consoleCursorPosition(float x, float y)
{
	cursorPosition.X = x;
	cursorPosition.Y = y;
	CONSOLE_CURSOR_INFO info;
	info.bVisible = false;
	info.dwSize = 1;
	SetConsoleCursorInfo(con[0], &info);
	SetConsoleCursorPosition(con[0], cursorPosition);
	//SetConsoleCursorInfo(con[1], &info);
	//SetConsoleCursorPosition(con[1], cursorPosition);
}

/*
toConsoleBuffer(string &str, float x, float y,float poX,float poY, Colour colour);
* str    - string to be drawn to buffer
* poX    - the x coord of origin position of the console (where you consoder (0,0) to be)
* poY    - the y coord of origin position of the console (where you consoder (0,0) to be)
* x      - x position from specified (0,0) coordinate on the screen
* y      - y position from specified (0,0) coordinate on the screen
* colour - colour of text

summary:
Adds single line to the console to the
specified (x,y) position from the specified top left corner
to be drawn after drawConsole(); is called (with text colour modification [see: enum colour in EmConsole.h])

*/
void Console::toConsoleBuffer(string &str, float poX, float poY, float x, float y, Colour colour)
{
	setSize(sz.X, sz.Y);
	consoleCursorPosition(0, 0);

	SMALL_RECT strSize;
	strSize.Top = y;
	strSize.Bottom = y + 1;
	strSize.Left = x;
	strSize.Right = x + str.size();

	ci = new CHAR_INFO[str.size()];
	for (int a = 0; a < str.size(); a++)
	{
		ci[a].Char.UnicodeChar = str[a];
		ci[a].Attributes = colour;
	}
	WriteConsoleOutputA(con[1], ci, {(SHORT)str.size(),1}, {(SHORT)poX,(SHORT)poY}, &strSize);
	delete[] ci;

}

/*
toConsoleBuffer(string &str, float x, float y, Colour colour);
* str    - string to be drawn to buffer
* x      - x position from specified (0,0) coordinate on the screen
* y      - y position from specified (0,0) coordinate on the screen
* colour - colour of text

summary:
Adds single line to the console to the
specified (x,y) position from the top left corner
to be drawn after drawConsole(); is called

*/
void Console::toConsoleBuffer(string &str, float x, float y, Colour colour)
{
	toConsoleBuffer(str, 0, 0, x, y, colour);
}

/*
toConsoleBuffer(string &str, float x, float y);
* str    - string to be drawn
* poX    - the x coord of origin position of the console (where you consoder (0,0) to be)
* poY    - the y coord of origin position of the console (where you consoder (0,0) to be)
* x      - x position from specified (0,0) coordinate on the screen
* y      - y position from specified (0,0) coordinate on the screen

summary:
Adds single line to the console to the
specified (x,y) position from the top left corner
to be drawn after drawConsole(); is called

*/
void Console::toConsoleBuffer(string &str, float x, float y)
{
	toConsoleBuffer(str, x, y, FG_WHITE);
}

/*
toConsoleBuffer(vector<string>& str, float x, float y, Colour colour);
* str    - vector of string to be drawn to buffer
* x      - x position from specified (0,0) coordinate on the screen
* y      - y position from specified (0,0) coordinate on the screen
* colour - colour of text

summary:
Adds multiple lines to the console starting at the
specified (x,y) position from the top left corner
to be drawn after drawConsole(); is called (with text colour modification [see: enum colour in EmConsole.h])
*/
void Console::toConsoleBuffer(vector<string>& str, float x, float y, Colour colour)
{
	for (auto a : str)
		toConsoleBuffer(a, x, y++, colour);
}

/*
toConsoleBuffer(vector<string>& str, float x, float y);
* str    - vector of string to be drawn to buffer
* x      - x position from specified (0,0) coordinate on the screen
* y      - y position from specified (0,0) coordinate on the screen

summary:
Adds multiple lines to the console starting at the
specified (x,y) position from the top left corner
to be drawn after drawConsole(); is called
*/
void Console::toConsoleBuffer(vector<string>& str, float x, float y)
{
	for (int a = 0; a < str.size(); a++)
		toConsoleBuffer(str[a], x, y++);
}

/*
toConsoleBuffer(string &str, float x, float y,float poX,float poY, Colour colour);
* str    - string to be drawn to buffer
* poX    - the x coord of origin position of the console (where you consoder (0,0) to be)
* poY    - the y coord of origin position of the console (where you consoder (0,0) to be)
* x      - x position from specified (0,0) coordinate on the screen
* y      - y position from specified (0,0) coordinate on the screen
* colour - colour of text

summary:
Adds single line to the console to the
specified (x,y) position from the specified top left corner
to be drawn after drawConsole(); is called (with text colour modification [see: enum colour in EmConsole.h])
*/
void Console::toConsoleBuffer(const char* str, float poX, float poY, float x, float y, Colour colour)
{
	toConsoleBuffer(string(str), poX, poY, x, y, colour);
}

/*
toConsoleBuffer(string &str, float x, float y, Colour colour);
* str    - string to be drawn to buffer
* x      - x position from specified (0,0) coordinate on the screen
* y      - y position from specified (0,0) coordinate on the screen
* colour - colour of text

summary:
Adds single line to the console to the
specified (x,y) position from the specified top left corner
to be drawn after drawConsole(); is called (with text colour modification [see: enum colour in EmConsole.h])

*/
void Console::toConsoleBuffer(const char *str, float x, float y, Colour colour)
{
	toConsoleBuffer(string(str), x, y, colour);
}

/*
toConsoleBuffer(string &str, float x, float y);
* str    - string to be drawn to buffer
* x      - x position from specified (0,0) coordinate on the screen
* y      - y position from specified (0,0) coordinate on the screen

summary:
Adds single line to the console to the
specified (x,y) position from the specified top left corner
to be drawn after drawConsole(); is called

*/
void Console::toConsoleBuffer(const char *str, float x, float y)
{
	toConsoleBuffer(string(str), x, y);
}

/*Draws the console (choose to clear console buffer after)*/
void Console::drawConsole(bool clear)
{
	
	if (!clear)
	{
		SetConsoleActiveScreenBuffer(con[0] = con[1]);
		
	} else
	{
		HANDLE temp = con[0];
		con[0] = con[1];
		con[1] = temp;
		SetConsoleActiveScreenBuffer(con[0]);
		count = 1;
		clearConsole();
		count = 0;
	}
}

/*Draws the console and clears the console buffer (clears console buffer after)*/
void Console::drawConsole()
{
	drawConsole(true);
}

/*Clears the console*/
void Console::clearConsole()
{
	DWORD cCharsWritten;
	FillConsoleOutputCharacterA(con[count], (TCHAR)' ', sz.X*sz.Y, COORD {0,0}, &cCharsWritten);
	FillConsoleOutputAttribute(con[count], 0, sz.X*sz.Y, COORD {0,0}, &cCharsWritten);
}