#pragma once
#include<Windows.h>
#include <fstream>
#include<string>
#include<vector>

class Console
{
public:
	/*Constructers*/
	Console(std::string title);

	Console();
	
	/*Enums*/
	enum Colour
	{
		FG_BLUE = FOREGROUND_BLUE,
		FG_GREEN = FOREGROUND_GREEN,
		FG_RED = FOREGROUND_RED,
		FG_WHITE = FOREGROUND_BLUE | FOREGROUND_GREEN | FOREGROUND_RED,
		FG_PURPLE = FOREGROUND_BLUE | FOREGROUND_RED,
		FG_CYAN = FOREGROUND_BLUE | FOREGROUND_GREEN,
		FG_YELLOW = FOREGROUND_GREEN | FOREGROUND_RED,
		FG_INTENSITY = FOREGROUND_INTENSITY,
		BG_BLUE = BACKGROUND_BLUE,
		BG_GREEN = BACKGROUND_GREEN,
		BG_RED = BACKGROUND_RED,
		BG_WHITE = BACKGROUND_BLUE | BACKGROUND_GREEN | BACKGROUND_RED,
		BG_PURPLE = BACKGROUND_BLUE | BACKGROUND_RED,
		BG_CYAN = BACKGROUND_BLUE | BACKGROUND_GREEN,
		BG_YELLOW = BACKGROUND_GREEN | BACKGROUND_RED,
		BG_INTENSITY = BACKGROUND_INTENSITY		
	};

	/*Functions*/

	std::vector<std::vector<std::string>> textFileToVector(std::string file);

	void setTitle(std::string title);

	void setSize(float x, float y);

	void consoleCursorPosition(float x, float y);

	void toConsoleBuffer(std::string & str, float poX, float poY, float x, float y, Colour colour);

	void toConsoleBuffer(std::string& str, float x, float y, Colour colour);

	void toConsoleBuffer(std::string& str, float x, float y);

	void toConsoleBuffer(std::vector<std::string>& str, float x, float y, Colour colour);

	void toConsoleBuffer(std::vector<std::string>& str, float x, float y);

	void toConsoleBuffer(const char * str, float poX, float poY, float x, float y, Colour colour);

	void toConsoleBuffer(const char * str, float x, float y, Colour colour);

	void toConsoleBuffer(const char * str, float x, float y);

	void drawConsole(bool clear);

	void drawConsole();

	void clearConsole();

	/*Variables*/
	SMALL_RECT size;
	CHAR_INFO *ci;
	COORD cursorPosition;
	HANDLE con[2];
	COORD sz = GetLargestConsoleWindowSize(con[count]);
	bool count=0;

};
